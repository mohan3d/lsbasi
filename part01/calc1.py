from __future__ import print_function
from six.moves import input

INTEGER, PLUS, MINUS, EOF = (
    'INTEGER', 'PLUS', 'MINUS', 'EOF'
)


class Token:
    def __init__(self, _type, value):
        self.type = _type
        self.value = value

    def __str__(self):
        return 'Token({}, {})'.format(
            self.type,
            self.value
        )

    __repr__ = __str__


class Interpreter:
    def __init__(self, text):
        self.text = text
        self.pos = 0
        self.current_token = None

    def skip_whitespaces(self):
        while self.pos < len(self.text) and self.text[self.pos].isspace():
            self.pos += 1

    def get_integer(self):
        text = self.text
        length = len(text)
        res = ''

        while self.pos < length:
            current_char = text[self.pos]
            if not current_char.isdigit():
                break

            res += current_char
            self.pos += 1

        return int(res)

    def get_next_token(self):
        text = self.text

        if self.pos >= len(text):
            return Token(EOF, None)

        self.skip_whitespaces()

        current_char = text[self.pos]

        if current_char.isdigit():
            token = Token(INTEGER, self.get_integer())
            return token

        if current_char == '+':
            token = Token(PLUS, current_char)
            self.pos += 1
            return token

        if current_char == '-':
            token = Token(PLUS, current_char)
            self.pos += 1
            return token

        # if reached here there is an error.
        raise Exception('Error parsing input')

    def eat(self, token_type):
        if self.current_token.type == token_type:
            self.current_token = self.get_next_token()
            return

        # if reached here there is an error
        raise Exception('Error parsing input')

    def expr(self):
        """
        expr = INTEGER PLUS INTEGER
        """
        self.current_token = self.get_next_token()

        left = self.current_token
        self.eat(INTEGER)

        op = self.current_token
        self.eat(PLUS)

        right = self.current_token
        self.eat(INTEGER)

        if op.type == PLUS:
            res = left.value + right.value
        elif op.type == MINUS:
            res = left.value - right.value
        else:
            raise Exception("Invalid operator")

        return res


def main():
    while True:
        try:
            expr = input('calc> ')
        except (EOFError, KeyboardInterrupt):
            break

        if not expr:
            continue

        interpreter = Interpreter(expr)
        result = interpreter.expr()
        print(result)


if __name__ == '__main__':
    main()
