from __future__ import print_function

import math

from six.moves import input

###############################################################################
#                                                                             #
#  LEXER                                                                      #
#                                                                             #
###############################################################################

# Token types
INTEGER, PLUS, MINUS, MUL, DIV, LPAREN, RPAREN, LN, SIN, COS, TAN, SQRT, POW, COMMA, EOF = (
    'INTEGER', 'PLUS', 'MINUS', 'MUL', 'DIV', '(', ')', 'LN', 'SIN', 'COS', 'TAN', 'SQRT', 'POW', 'COMMA', 'EOF'
)


class Token(object):
    def __init__(self, _type, value):
        self.type = _type
        self.value = value

    def __str__(self):
        """String representation of the class instance.
        Examples:
            Token(INTEGER, 3)
            Token(PLUS, '+')
            Token(MUL, '*')
        """
        return 'Token({type}, {value})'.format(
            type=self.type,
            value=repr(self.value)
        )

    def __repr__(self):
        return self.__str__()


class Lexer(object):
    def __init__(self, text):
        # client string input, e.g. "4 + 2 * 3 - 6 / 2"
        self.text = text
        # self.pos is an index into self.text
        self.pos = 0
        self.current_char = self.text[self.pos]

    def error(self):
        raise Exception('Invalid character')

    def advance(self):
        """Advance the `pos` pointer and set the `current_char` variable."""
        self.pos += 1
        if self.pos > len(self.text) - 1:
            self.current_char = None  # Indicates end of input
        else:
            self.current_char = self.text[self.pos]

    def skip_whitespace(self):
        while self.current_char is not None and self.current_char.isspace():
            self.advance()

    def integer(self):
        """Return a (multidigit) integer consumed from the input."""
        result = ''
        while self.current_char is not None and self.current_char.isdigit():
            result += self.current_char
            self.advance()
        return int(result)

    def is_function(self, function_name):
        """Check if token matches a function name (ln, cos, tan, sin, ..), return it if matched"""
        n = len(function_name)

        if self.pos + n > len(self.text):
            return False

        for i in range(n):
            if function_name[i] != self.text[i + self.pos]:
                return False

        return True

    def get_next_token(self):
        """Lexical analyzer (also known as scanner or tokenizer)
        This method is responsible for breaking a sentence
        apart into tokens. One token at a time.
        """
        while self.current_char is not None:

            if self.current_char.isspace():
                self.skip_whitespace()
                continue

            if self.current_char.isdigit():
                return Token(INTEGER, self.integer())

            if self.current_char == '+':
                self.advance()
                return Token(PLUS, '+')

            if self.current_char == '-':
                self.advance()
                return Token(MINUS, '-')

            if self.current_char == '*':
                self.advance()
                return Token(MUL, '*')

            if self.current_char == '/':
                self.advance()
                return Token(DIV, '/')

            if self.current_char == '(':
                self.advance()
                return Token(LPAREN, '(')

            if self.current_char == ')':
                self.advance()
                return Token(RPAREN, ')')

            if self.current_char == 'l':
                if self.is_function('ln'):
                    self.advance()
                    self.advance()
                    return Token(LN, 'ln')

            if self.current_char == 's':
                if self.is_function('sin'):
                    self.advance()
                    self.advance()
                    self.advance()
                    return Token(SIN, 'sin')
                elif self.is_function('sqrt'):
                    self.advance()
                    self.advance()
                    self.advance()
                    self.advance()
                    return Token(SQRT, 'sqrt')

            if self.current_char == 'c':
                if self.is_function('cos'):
                    self.advance()
                    self.advance()
                    self.advance()
                    return Token(COS, 'cos')

            if self.current_char == 't':
                if self.is_function('tan'):
                    self.advance()
                    self.advance()
                    self.advance()
                    return Token(TAN, 'tan')

            if self.current_char == 'p':
                if self.is_function('pow'):
                    self.advance()
                    self.advance()
                    self.advance()
                    return Token(POW, 'pow')

            if self.current_char == ',':
                self.advance()
                return Token(COMMA, ',')

            self.error()

        return Token(EOF, None)


###############################################################################
#                                                                             #
#  PARSER                                                                     #
#                                                                             #
###############################################################################

class AST(object):
    pass


class BinOp(AST):
    def __init__(self, left, op, right):
        self.left = left
        self.token = self.op = op
        self.right = right


class UnaryOp(AST):
    def __init__(self, op, expr):
        self.op = op
        self.expr = expr


class FunctionOp(AST):
    def __init__(self, func, expr):
        self.func = func
        self.expr = expr


class BinaryFunctionOp(AST):
    def __init__(self, func, left_expr, right_expr):
        self.func = func
        self.left_expr = left_expr
        self.right_expr = right_expr


class Num(AST):
    def __init__(self, token):
        self.token = token
        self.value = token.value


class Parser(object):
    def __init__(self, lexer):
        self.lexer = lexer
        # set current token to the first token taken from the input
        self.current_token = self.lexer.get_next_token()

    def error(self):
        raise Exception('Invalid syntax')

    def eat(self, token_type):
        if self.current_token.type == token_type:
            self.current_token = self.lexer.get_next_token()
        else:
            self.error()

    def factor(self):
        # """factor : INTEGER | LPAREN expr RPAREN"""
        """factor : (PLUS | MINUS) factor | INTEGER | LPAREN expr RPAREN"""
        token = self.current_token
        ttype = token.type

        if ttype in (PLUS, MINUS):
            """1 + 2"""
            self.eat(token.type)
            node = UnaryOp(token, self.factor())
            return node

        elif ttype in (LN, SIN, COS, TAN, SQRT):
            """ln(10), sqrt(1 * 2 * 3 * 4 * (1 + 2))"""
            self.eat(token.type)
            node = FunctionOp(token, self.factor())
            return node

        elif ttype == POW:
            """pow(2, 4), pow(sin(90) * 2, (1 + 2 * 2 - 1))
            POW = pow LPAREN factor COMMA factor RPAREN
            """
            self.eat(POW)
            self.eat(LPAREN)
            left = self.expr()
            self.eat(COMMA)
            right = self.expr()
            self.eat(RPAREN)
            return BinaryFunctionOp(token, left, right)

        elif ttype == INTEGER:
            self.eat(INTEGER)
            return Num(token)

        elif ttype == LPAREN:
            self.eat(LPAREN)
            node = self.expr()
            self.eat(RPAREN)
            return node

    def term(self):
        """term : factor ((MUL | DIV) factor)*"""
        node = self.factor()

        while self.current_token.type in (MUL, DIV):
            token = self.current_token
            if token.type == MUL:
                self.eat(MUL)
            elif token.type == DIV:
                self.eat(DIV)

            node = BinOp(left=node, op=token, right=self.factor())

        return node

    def expr(self):
        """
        expr   : term ((PLUS | MINUS) term)*
        term   : factor ((MUL | DIV) factor)*
        factor : INTEGER | LPAREN expr RPAREN
        """
        node = self.term()

        while self.current_token.type in (PLUS, MINUS):
            token = self.current_token
            if token.type == PLUS:
                self.eat(PLUS)
            elif token.type == MINUS:
                self.eat(MINUS)

            node = BinOp(left=node, op=token, right=self.term())

        return node

    def parse(self):
        node = self.expr()
        if self.current_token.type != EOF:
            self.error()
        return node


###############################################################################
#                                                                             #
#  INTERPRETER                                                                #
#                                                                             #
###############################################################################

class NodeVisitor(object):
    def visit(self, node):
        method_name = 'visit_' + type(node).__name__
        visitor = getattr(self, method_name, self.generic_visit)
        return visitor(node)

    def generic_visit(self, node):
        raise Exception('No visit_{} method'.format(type(node).__name__))


class Interpreter(NodeVisitor):
    def __init__(self, parser):
        self.parser = parser

    def visit_BinOp(self, node):
        if node.op.type == PLUS:
            return self.visit(node.left) + self.visit(node.right)
        elif node.op.type == MINUS:
            return self.visit(node.left) - self.visit(node.right)
        elif node.op.type == MUL:
            return self.visit(node.left) * self.visit(node.right)
        elif node.op.type == DIV:
            return self.visit(node.left) / self.visit(node.right)

    def visit_UnaryOp(self, node):
        if node.op.type == PLUS:
            return self.visit(node.expr)
        elif node.op.type == MINUS:
            return - self.visit(node.expr)

    def visit_FunctionOp(self, node):
        ftype = node.func.type

        if ftype == LN:
            return math.log(self.visit(node.expr))
        elif ftype == SIN:
            return math.sin(math.radians(self.visit(node.expr)))
        elif ftype == COS:
            return math.cos(math.radians(self.visit(node.expr)))
        elif ftype == TAN:
            return math.tan(math.radians(self.visit(node.expr)))
        elif ftype == SQRT:
            return math.sqrt(self.visit(node.expr))

    def visit_BinaryFunctionOp(self, node):
        if node.func.type == POW:
            return math.pow(self.visit(node.left_expr), self.visit(node.right_expr))

    def visit_Num(self, node):
        return node.value

    def interpret(self):
        tree = self.parser.parse()
        return self.visit(tree)


class PostFixNotionInterpreter(Interpreter):
    def visit_BinOp(self, node):
        return "{} {} {}".format(
            self.visit(node.left),
            self.visit(node.right),
            node.op.value
        )


class ListStyleInterpreter(Interpreter):
    def visit_BinOp(self, node):
        return "({} {} {})".format(
            node.op.value,
            self.visit(node.left),
            self.visit(node.right)
        )


def main():
    while True:
        try:
            text = input('spi> ')
        except (KeyboardInterrupt, EOFError):
            break

        if not text:
            continue

        lexer = Lexer(text)
        parser = Parser(lexer)
        interpreter = Interpreter(parser)
        # interpreter = PostFixNotionInterpreter(parser)
        # interpreter = ListStyleInterpreter(parser)
        result = interpreter.interpret()
        print(result)


if __name__ == '__main__':
    main()
